Tree Manager
=============

Tree Manager is Nette Framework based application to demonstrate work with Preorder Tree Traversal.

Installing
----------

- Create PostgreSQL database.
- Execute /tree-manager.sql script.
- Update database credentials in /app/config/config.local.neon if necessary.
- Make directories `/temp` and `/log` writable.
- Navigate your browser to the `/www` directory.

License
-------
- Nette: New BSD License or GPL 2.0 or 3.0 (http://nette.org/license)
