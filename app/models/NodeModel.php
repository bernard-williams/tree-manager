<?php

namespace App\Models;

use Nette;


class NodeModel extends Nette\Object {
	/** @var Nette\Database\Context */
	private $database;

	const    SEQUENCE = 'tm_nodes_id_seq';

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}


	/**
	 * Select table
	 *
	 * @return Nette\Database\Table\Selection
	 */
	public function table() {
		return $this->database->table('tm_nodes');
	}

	/**
	 * Return all ordered nodes
	 *
	 * @return Nette\Database\Table\Selection
	 */
	public function getNodes() {
		return $this->table()->order('depth, lft')->fetchAll();
	}

	/**
	 * Return node by ID
	 *
	 * @param int $id Node ID
	 * @throws InvalidParameterException
	 * @throws NodeNotFoundException
	 * @return Nette\Database\Table\IRow
	 */
	public function findById($id) {
		$this->isId($id);

		$parent = $this->table()->where('id', $id)->fetch();
		if (!$parent) {
			throw new NodeNotFoundException('Node has not been found.');
		}

		return $parent;
	}

	/**
	 * Return all nodes by IDs
	 *
	 * @param array $ids An array of node IDs
	 * @return array
	 */
	public function findAffectedNodes($ids) {
		$data = array();

		$rows = $this->table()->where('id IN (?)', $ids)->fetchAll();
		foreach ($rows as $row) {
			$data[] = (object) $row->toArray();
		}

		return $data;
	}

	/**
	 * Insert a child node
	 *
	 * @param int $parentId Parent node ID
	 * @throws NodeNotFoundException
	 * @return array Return an information about a new node and all the affected nodes
	 */
	public function insert($parentId = NULL) {
		$this->database->beginTransaction();

		if ($parentId) { // child
			try {
				$node = $this->findById($parentId);
			} catch (NodeNotFoundException $e) {
				throw new NodeNotFoundException('Parent node has not been found.');
			}

			$values = array(
					'parent_id' => $parentId,
					'depth' => $node->depth + 1,
					'lft' => $node->rgt,
					'rgt' => $node->rgt + 1,
			);

			// get affected node IDs first
			$affectedIds = $this->getAffectedIds('lft > ? OR rgt >= ?', array($node->rgt, $node->rgt));

			// then update nodes
			$this->table()->where('lft > ?', $node->rgt)->update(array('lft' => new Nette\Database\SqlLiteral('lft + 2')));
			$this->table()->where('rgt >= ?', $node->rgt)->update(array('rgt' => new Nette\Database\SqlLiteral('rgt + 2')));

		} else { // root node
			$lastRgt = $this->getMaxRgt();

			$affectedIds = array();
			$values = array(
					'parent_id' => NULL,
					'depth' => 0,
					'lft' => $lastRgt + 1,
					'rgt' => $lastRgt + 2,
			);
		}

		$this->table()->insert($values);
		$values['id'] = $this->getInsertedId();

		$this->database->commit();

		return array(
				'node' => (object) $values,
				'affected' => $affectedIds ? $this->findAffectedNodes($affectedIds) : array(),
		);
	}

	/**
	 * Return biggest rgt
	 *
	 * @return int
	 */
	public function getMaxRgt() {
		return (int) $this->table()->select('MAX(rgt)')->fetchField();
	}

	/**
	 * Return an array of node IDs which will be affected by next operation
	 *
	 * @param string $whereStatement SQL WHERE statement to execute
	 * @param array|string $whereParams All the necessary parameters
	 * @return array
	 */
	public function getAffectedIds($whereStatement, $whereParams = array()) {
		$rows = $this->table()->where($whereStatement, $whereParams)->select('id')->fetchPairs('id');

		return array_keys($rows);
	}

	/**
	 * Delete node
	 *
	 * @param int $id Node ID to delete
	 * @throws NodeNotFoundException
	 * @throws InvalidParameterException
	 * @return array Return an information about deleted nodes and all the affected nodes
	 */
	public function delete($id) {
		$parent = $this->findById($id);

		$this->database->beginTransaction();

		// select IDs to delete first
		$rows = $this->table()->where('lft >= ? AND rgt <= ?', array($parent->lft, $parent->rgt))->select('id');
		$deletedIds = $rows->fetchPairs('id');

		// then delete nodes
		$rows->delete();

		// get affected node IDs first
		$affectedIds = $this->getAffectedIds('lft > ? OR rgt > ?', array($parent->rgt, $parent->rgt));

		// then update nodes
		$diff = $parent->rgt - $parent->lft + 1;
		$this->table()->where('lft > ?', $parent->rgt)->update(array('lft' => new Nette\Database\SqlLiteral('lft - ' . $diff)));
		$this->table()->where('rgt > ?', $parent->rgt)->update(array('rgt' => new Nette\Database\SqlLiteral('rgt - ' . $diff)));

		$this->database->commit();

		return array(
				'deleted' => array_keys($deletedIds),
				'affected' => $affectedIds ? $this->findAffectedNodes($affectedIds) : array(),
		);
	}

	/**
	 * Determines whether input is an integer. Integer as a string is also accepted.
	 *
	 * @param mixed $id Value to verify
	 * @throws InvalidParameterException
	 * @return bool
	 */
	public function isId($id) {
		if (!Nette\Utils\Validators::isNumericInt($id)) {
			throw new InvalidParameterException('Node ID is not an integer.');
		}

		return TRUE;
	}

	/**
	 * Return last inserted ID
	 *
	 * @return int
	 */
	public function getInsertedId() {
		return (int) $this->database->getInsertId(self::SEQUENCE);
	}
}

class TreeException extends \Exception {
}

class NodeNotFoundException extends TreeException {
}

class InvalidParameterException extends TreeException {
}