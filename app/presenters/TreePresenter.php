<?php

namespace App\Presenters;

use App\Models\NodeModel;
use App\Models\TreeException;
use Nette;


class HomepagePresenter extends Nette\Application\UI\Presenter {
	/** @var NodeModel @inject */
	public $model;

	const    STATUS_OK = 'ok',
			STATUS_ERROR = 'error';

	/**
	 * Render tree
	 */
	public function renderDefault() {
		$this->template->nodes = $this->model->getNodes();
	}

	/**
	 * Add a new node
	 *
	 * @param int|null $id    Parent ID. If is not specified root node will be created.
	 */
	public function actionAdd($id = NULL) {
		try {
			$this->sendSuccess($this->model->insert((int) $id));
		} catch (TreeException $e) {
			$this->sendError($e->getMessage());
		}
	}

	/**
	 * Delete a node
	 *
	 * @param int $id    Node ID to delete
	 */
	public function actionDelete($id) {
		try {
			$this->sendSuccess($this->model->delete((int) $id));
		} catch (TreeException $e) {
			$this->sendError($e->getMessage());
		}
	}


	/**
	 * Send success response
	 *
	 * @param array $payload    Data to send back
	 */
	public function sendSuccess($payload = array()) {
		$data = array(
				'status' => self::STATUS_OK,
				'data' => (object) $payload,
		);

		echo Nette\Utils\Json::encode($data);
		$this->terminate();
	}


	/**
	 * Send response with error.
	 *
	 * @param string   $errorMessage Error message
	 */
	public function sendError($errorMessage = '') {
		$data = array(
				'error' => $errorMessage,
				'status' => self::STATUS_ERROR,
		);

		echo Nette\Utils\Json::encode($data);
		$this->terminate();
	}

}
