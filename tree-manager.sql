--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.0
-- Dumped by pg_dump version 9.1.0
-- Started on 2016-11-28 23:39:47

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 163 (class 3079 OID 11638)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

-- CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1857 (class 0 OID 0)
-- Dependencies: 163
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

-- COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 162 (class 1259 OID 48913)
-- Dependencies: 5
-- Name: tm_nodes; Type: TABLE; Schema: public; Owner: -; Tablespace:
--

CREATE TABLE tm_nodes (
    id integer NOT NULL,
    depth integer,
    parent_id integer,
    lft integer NOT NULL,
    rgt integer NOT NULL
);


--
-- TOC entry 161 (class 1259 OID 48911)
-- Dependencies: 5 162
-- Name: tm_nodes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tm_nodes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1858 (class 0 OID 0)
-- Dependencies: 161
-- Name: tm_nodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tm_nodes_id_seq OWNED BY tm_nodes.id;


--
-- TOC entry 1848 (class 2604 OID 48916)
-- Dependencies: 161 162 162
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE tm_nodes ALTER COLUMN id SET DEFAULT nextval('tm_nodes_id_seq'::regclass);


--
-- TOC entry 1850 (class 2606 OID 48922)
-- Dependencies: 162 162
-- Name: tm_nodes_id; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace:
--

ALTER TABLE ONLY tm_nodes
    ADD CONSTRAINT tm_nodes_id PRIMARY KEY (id);


--
-- TOC entry 1851 (class 1259 OID 48952)
-- Dependencies: 162
-- Name: tm_nodes_lft; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX tm_nodes_lft ON tm_nodes USING btree (lft);


--
-- TOC entry 1852 (class 1259 OID 48953)
-- Dependencies: 162
-- Name: tm_nodes_rgt; Type: INDEX; Schema: public; Owner: -; Tablespace:
--

CREATE INDEX tm_nodes_rgt ON tm_nodes USING btree (rgt);


-- Completed on 2016-11-28 23:39:48

--
-- PostgreSQL database dump complete
--

