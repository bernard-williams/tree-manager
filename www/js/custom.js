(function($, window, document) {
	$(function() {
		// set path
		var basePath = $('base').attr('href');

		var actionAdd = basePath + '/add';
		var actionDelete = basePath + '/delete';

		var elButtonAdd = $('#button-add-node');
		var elInputParentId = $('#input-parent-id');

		// add a node
		elButtonAdd.on('click', function(event) {
			event.preventDefault();
			clearErrorMessages();

			var id = elInputParentId.val();

			// verify input data
			if (id != '' && !verifyInt(id)) {
				return false;
			}

			$.post(actionAdd + '/' + id, function (response) {
				// validate server response
				if (response.status == 'error') {
					displayAlert(response.error);
					return null;
				}

				// update affected nodes
				if (response.data.affected.length) {
					updateAffected(response.data.affected);
				}

				// add a new node
				addNode(response.data.node);

				elInputParentId.val('');
			}, 'json');

			return false;
		});

		// delete a node
		$('body').on('click', '.node-button-delete', function(event) {
			event.preventDefault();
			clearErrorMessages();

			var id = $(this).attr('data-id');

			$.post(actionDelete + '/' + id, function (response) {
				// validate server response
				if (response.status == 'error') {
					displayAlert(response.error);
					return null;
				}

				// update affected nodes
				if (response.data.affected.length) {
					updateAffected(response.data.affected);
				}

				// remove nodes
				deleteNode(response.data.deleted);

			}, 'json');

			return false;
		});

	});

	// AJAX common error handler
	$(document).ajaxError(function () {
		displayAlert('The server encountered an internal error and was unable to complete your request. Please try again later.');
	});

	/**
	 * Update all the affected nodes
	 * @param {Array} items Nodes to update
	 */
	function updateAffected(items) {
		for (var i in items) {
			if (items.hasOwnProperty(i)) {
				$('#node' + items[i].id).attr('data-rgt', items[i].rgt).children('.tree-node-info').html(items[i].lft + ' <span class="tree-node-id label label-primary">' + items[i].id + '</span> ' + items[i].rgt);
			}
		}
	}

	/**
	 * Add a new node to the DOM
	 * @param {Object} node Node to add
	 */
	function addNode(node) {
		var treeRow = $('#tree-row-' + node.depth);
		if (treeRow.length) { // row container already exists
			var rgt = findRgt(treeRow, node.lft);
			if (rgt == 0) { // add a node at the beginning
				treeRow.children('.tree-depth').after(buildNode(node));
			} else { // add a node behind the a specific node
				treeRow.children(".tree-node[data-rgt='" + rgt + "']").after(buildNode(node));
			}
		} else { // create a new tree row
			$('#tree-container').append(buildRow(node));
		}
	}

	/**
	 * Remove nodes from DOM
	 * @param {Array} ids List of node IDs to delete
	 */
	function deleteNode(ids) {
		for (var i in ids) {
			if (ids.hasOwnProperty(i)) {
				$('#node' + ids[i]).remove();
			}
		}

		// remove rows without any node
		$('.tree-row').each(function(index, obj) {
			var el = $(obj);
			if (!el.children('.tree-node').length) {
				el.remove();
			}
		});
	}

	/**
	 * Find required rgt according to node lft
	 * @param {Object} row Tree row to scan
	 * @param {int|string} lft Node lft
	 */
	function findRgt(row, lft) {
		var nodeLft = parseInt(lft, 10);
		var elRgt;
		var rgt = 0;

		row.children('.tree-node').each(function(index, obj) {
			elRgt = parseInt($(obj).attr('data-rgt'), 10);
			if (elRgt < nodeLft) {
				rgt = Math.max(rgt, elRgt);
			}
		});

		return rgt;
	}

	/**
	 * Build a HTML of the node
	 * @param {Object} node Information about the node
	 */
	function buildNode(node) {
		var html = '<div class="tree-node" id="node' + node.id + '" data-rgt="' + node.rgt + '">'
				+ ' <button type="button" class="node-button-delete close" data-id="' + node.id + '" aria-label="Delete"><span aria-hidden="true">&times;</span></button>';

		if (node.parent_id) {
			 html += ' <span class="tree-node-parent badge">' + node.parent_id + '</span><br>';
		}

		html += ' <span class="tree-node-info">' + node.lft + ' <span class="tree-node-id label label-primary">' + node.id + '</span> ' + node.rgt + '</span>'
				+ ' </div>';

		return html;
	}

	/**
	 * Build a HTML of the tree row
	 * @param {Object} node Information about the node
	 */
	function buildRow(node) {
		return '<div class="tree-row" id="tree-row-' + node.depth + '"><div class="tree-depth">' + node.depth + '</div>' + buildNode(node) + '</div>';
	}

	/**
	 * Display (prepend) an alert to specific container
	 * @param {String} message Message to display
	 */
	function displayAlert(message) {
		$('#messages-container').prepend('<div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + message + '</div>')
	}

	/**
	 * Remove all error messages
	 */
	function clearErrorMessages() {
		$('.alert').remove();
	}

	/**
	 * Determines whether value is an integer
	 * @param {String} value Value to verify
	 * @return {boolean}
	 */
	function verifyInt(value) {
		var pattern = /^\d+$/g;

		if (!pattern.test(value)) {
			displayAlert('Please enter a valid integer.');
			return false;
		}

		return true;
	}

}(window.jQuery, window, document));